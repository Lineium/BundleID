//
//  CustomCell.swift
//  BundleID
//
//  Created by Diego Quiroz García on 12/23/15.
//  Copyright © 2015 Botgard. All rights reserved.
//

import Foundation
import UIKit
import Parse
import ParseUI

class CustomCell:  PFTableViewCell {
    
    @IBOutlet weak var customName: UILabel!
    @IBOutlet weak var customID: UILabel!
    @IBOutlet weak var BundleIcon: PFImageView!
    
}