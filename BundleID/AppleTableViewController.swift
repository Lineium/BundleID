//
//  AppleTableViewController.swift
//  BundleID
//
//  Created by Botgard on 12/21/15.
//  Copyright © 2015 Botgard. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class AppleTableViewController: PFQueryTableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchResults = [String]()
    
    override func viewDidAppear(animated: Bool) {
        searchBar.delegate = self
    }
    
    
    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
        searchBar.delegate = self
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // Configure the PFQueryTableView
        self.parseClassName = "Apple"
        self.textKey = "Name"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
    }
    
    // Define the query that will provide the data for the table view
    override func queryForTable() -> PFQuery {
        let query = PFQuery(className: "Apple")
        query.orderByAscending("Name")
        query.limit = 1000
        return query
    }
    
    //override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("appleCell") as! PFTableViewCell!
        if cell == nil {
            cell = PFTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "appleCell")
        }
        
        // Extract values from the PFObject to display in the table cell
        if let Name = object?["Name"] as? String {
            cell?.textLabel?.text = Name
        }
        if let ID = object?["ID"] as? String {
            cell?.detailTextLabel?.text = ID
        }
        
        return cell
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        let AppleQuery = PFQuery(className: "Apple")
        AppleQuery.whereKey("Name", equalTo: searchBar.text!)
        
        AppleQuery.getFirstObjectInBackgroundWithBlock {
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                searchBar.endEditing(true)
            } else {
                // The find succeeded.
                print("Successfully retrieved the object.")
                print(object)
                searchBar.endEditing(true)
                
                
                let alertController = UIAlertController(title: object!["Name"] as? String, message:
                    object!["ID"] as? String, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}